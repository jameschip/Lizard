# THE LIZARD

The lizard is a small markup language created to avoid the verbosity of writing HTML 
while having a more consistent syntax than markdown. All tags used in The Lizard use 
the same format, the following tag will put the text "the tag data" in bold.

{{ bo the tag data }}

This repo contains the small spec for The Lizard markup language and also a small tool 
for parsing text files written using The Lizard and converting them to HTML.

## THE SYNTAX.

### HEADER

There can be a small header in a liz file that provides metadata about the file contents.

To create a header the first line of your file should be **~~~**. After that each line holdsone pice of metadata,the type of which is indicated by the first character of the line.

A header is then closed with **~~~** again.

Example header:  
~~~  
O outputFileName.html  
D Description of content.  
T Title of page.  
~~~  

### TAGS

Tags can NOT be nested. This is by design to promote consistent style use. The only 
syntax that does not use the tag format is the single newline that can be used to inset 
a line break into a paragraph and the double line break that can be used to start a 
new paragraph.

### PARAGRAPHS
This is a paragraph with some text. Two newlines will start a new paragraph.  
  
This is another paragraph. They are separated by a blank line.  
Just putting a single newline will insert a line break.


### TEXT STYLE
{{ bo bold text }}  
{{ it italic text }}


### LINKS
{{ li https://jameschip.io link text }}


### IMAGES
{{ im image/location.png }}


### BULLET POINTS
Text before bullet points....  
{{ bp The first bullet point }}  
{{ bp The second bullet point }}  
{{ bp The Third bullet point }}  
Text after bullet points...


### BLOCK QUOTES
{{ bq this is a quote }}


### CODE BLOCKS
{{ cb  
This is  
   some  
   preformatted  
text  
}}  


### SKIPPING TAGS
/{{ bo this tag will be skipped and displayed as text }}


## LIZ

Liz is a small tool for converting text in The Lizard format into HTML. 
It is recommended, but not mandatory, to give files using The Lizard 
syntax the extension '.liz'.

The template file is an htmll file with a line that contains only the text **{{content}}** in it. Lizard will replace the tag with the generated html from converting your lizard markup.

### COMPILE
gcc lizard.c parser.c -o liz

### USE

liz -i fileToParse.liz -d outputDirectory -t templateFile



