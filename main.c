#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include "parser.h"

char * out_dir = NULL;
char * input_file = NULL;
char * template_file = NULL;
char * out_file = NULL;
char * title = NULL;
char * description = NULL;
char tags[ 10 ][ 1000 ];
int tag_count = 0;

FILE * open_file( const char * file, const char * rw ) {
    
    if ( file == NULL ) {
        printf( "Unable to open file -> %s.\n", file );
        exit( EXIT_FAILURE) ;
    }

    FILE * fp = fopen( file, rw );
    if ( fp == NULL ) {
        printf( "Unable to access %s.\n", file );
        exit( EXIT_FAILURE );
    }

    return fp;
    
}

void parse_header( FILE * fp ) {

    size_t len = 1000;

    char * line = ( char * ) malloc( len );
    if ( line == NULL ) {
        printf( "Unable to allocate memory for header parse.\n" );
        exit( EXIT_FAILURE );
    }

    getline( &line, &len, fp );
        
    if ( strcmp( line, "~~~\n" ) != 0 ) {
        return;
    }

    while ( getline( &line, &len, fp ) > 0 ) {
        int index = 0;

        if ( strcmp( line, "~~~\n" ) == 0 ) {
            return;
        }

        switch ( line[ 0 ] ) {
            
            case 'T':
                while ( line[ ++index ] == ' ' );
                char * title = ( char * ) malloc( len );
                if ( title == NULL ) {
                    printf(  "error assigning memory for out file.\n" );
                    exit( EXIT_FAILURE );
                }
                strcpy( title, &line[ index ] );        
                break;
            
            case 'O':
                while ( line[ ++index ] == ' ' );
                char * out_dir = ( char * ) malloc( len );
                if ( out_dir == NULL ) {
                    printf(  "error assigning memory for out file.\n" );
                    exit( EXIT_FAILURE );
                }
                strcpy( out_dir, &line[ index ] );
                break;

            case 'D':
                while ( line[ ++index ] == ' ' );
                char * description = ( char * ) malloc( len );
                if ( description == NULL ) {
                    printf(  "error assigning memory for out file.\n" );
                    exit( EXIT_FAILURE );
                }
                strcpy( description, &line[ index ] );
                break;

            case '#':
                while ( line[ ++index ] == ' ' );
                strcpy( tags[ tag_count++ ], &line[ index ] );
                break;

            default:
                break;
        }

    }

}

void create_out_dir( void ) {

    if ( out_dir == NULL ) {
        return;
    }

    DIR* dir = opendir( out_dir );
    if (dir) {
        closedir(dir);
        return;
    } else if (ENOENT == errno) {
        if ( mkdir( out_dir, S_IRWXU ) < 0 ) {
            perror( "Outout dir error " );
            exit( EXIT_FAILURE );
        }
    } else {
        printf( "Unable to create output directory.\n" );
        exit( EXIT_FAILURE );
    }

}

void create_out_file_string( void ) {

    if ( out_file == NULL ) {
        out_file = ( char* ) malloc( 1000 );
        if ( out_file == NULL ) {
            printf( "Unable to alloc memory -> create_output_file_string.\n" );
            exit( EXIT_FAILURE );
        }
        strcpy( out_file, input_file );
        char * dt = strrchr( out_file, '.' );
        strcpy( dt + 1, "html" );
    }

    if ( out_dir != NULL ) {
        char tmp[ 1000 ];
        sprintf( tmp, "%s%s", out_dir, out_file);
        strcpy( out_file, tmp );
    }

}

void output_to_content_tag( FILE * ofp, FILE * ifp ) {
    
    char str[ 10000 ];
    int index = 0;
    int c;

    do {
        c = fgetc( ifp );
        if ( (char)c == '{' ) {
            if ( ( c = fgetc( ifp )) == '{' ) {
                while ( ( c= fgetc( ifp )) != '}' );
                while ( ( c= fgetc( ifp )) != '}' );
                break;                
            } else {
                    str[ index++ ] = '{';
            }
        }

        str[ index++ ]= ( char ) c;

    } while ( c != EOF );

    str[ index++] = '\0';
    fputs( str, ofp );

}

void copy_file_content( FILE * ofp, FILE * ifp ) {
    int ch;
    char str[ 10000 ];
    int index = 0;

    do {
        ch = fgetc( ifp );
        str[ index++ ] = (char)ch;
    } while ( ch != EOF );
    fputs( str, ofp );
}

int main( int argc, char ** argv) {

    FILE * ifp = NULL;
    FILE * ofp = NULL;
    FILE * tfp = NULL;

    int sl = 0;
    int opt;

    while ((opt = getopt(argc, argv, "i:d:t:")) != -1) {
        switch (opt) {
            case 'i':
                sl = strlen( optarg );
                input_file = ( char * )malloc( sl + 1 );
                if ( input_file == NULL ) {
                    printf( "Error malloc input.\n" );
                    exit( EXIT_FAILURE );
                }
                sl = 0;
                strcpy( input_file, optarg );
                break;

            case 'd':
                sl = strlen( optarg );
                out_dir = ( char * )malloc( 1000 );
                if ( out_dir == NULL ) {
                    printf( "Error malloc out.\n" );
                    exit( EXIT_FAILURE );
                }
                if ( optarg[ sl ] != '/' ) {
                    sprintf( out_dir, "%s/", optarg );
                } else {
                    strcpy( out_dir, optarg );
                }
                break;

            case 't':
                sl = strlen( optarg );
                template_file = ( char * )malloc( sl + 1 );
                if ( template_file == NULL ) {
                    printf( "Error malloc input.\n" );
                    exit( EXIT_FAILURE );
                }
                sl = 0;
                strcpy( template_file, optarg );
                break;
            
            default: /* '?' */
                printf( "Unknown argument.\n" );
                exit( EXIT_FAILURE );
        }
    }

    // parse liz header
    ifp = open_file( input_file, "r" );
    parse_header( ifp );

    // create output file
    create_out_dir();
    create_out_file_string();
    ofp = open_file( out_file, "w" );

    // output template to file up to content tag

    if ( template_file != NULL ) {
        tfp = open_file( template_file, "r" );
        output_to_content_tag( ofp, tfp );
    }
    
    // parse liz
    char liz[ 10000 ];
    ifp = open_file( input_file, "r" );
    int c, index= 0;
    while ( (c = fgetc( ifp )) != EOF ) {
        liz[ index++ ] = (char)c;
    }
    liz[ index ] = '\0';

    char * lop = parse( liz );
    
    // output liz to file
    fputs( lop, ofp );

    // output rest of template to fike.
    if ( template_file != NULL ) {
        copy_file_content( ofp, tfp );
    }

    free( input_file );
    free( out_dir );
    free( template_file );  
    free( out_file );

}

